from sqlalchemy import Column, Integer, Boolean, DATETIME
from datetime import datetime
from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login
from hashlib import md5



@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class Token(db.Model):

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.Integer(), nullable = False, unique = True)
    served = db.Column(db.Boolean, default = True, nullable = False)

    def __repr__(self):
        return f'<Token {self.name!r}>'

class Timings(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.Integer(), nullable = False, unique = True)
    event = db.Column(db.Boolean, nullable = False)
    time = db.Column(db.DATETIME, default = datetime.utcnow)

    def __repr__(self):
        return f'<Timings {self.name!r}>'

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    about_me = db.Column(db.String(200))   
    last_seen  = db.Column(db.DATETIME, default = datetime.utcnow)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)


    

